package cn.ts.config;


import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.annotation.Resource;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author: ts
 * @description
 * @create: 2025/1/6 10:42
 */

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    @Bean
    public ApplicationRunner applicationRunner(ThreadPoolExecutor threadPoolExecutor01) {
        return args -> {
            new Thread(() -> {
                while (true) {
                    Random random = new Random();
                    int initialDelay = random.nextInt(10) + 1;
                    int sleepTime = random.nextInt(10) + 1;

                    threadPoolExecutor01.submit(() -> {
                        try {
                            TimeUnit.SECONDS.sleep(initialDelay);
                            System.out.println("Task started after " + initialDelay + " seconds.");

                            TimeUnit.SECONDS.sleep(sleepTime);
                            System.out.println("Task executed for " + sleepTime + " seconds.");
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                        }
                    });

                    try {
                        Thread.sleep(random.nextInt(50) + 1);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }).start();
        };
    }

}
