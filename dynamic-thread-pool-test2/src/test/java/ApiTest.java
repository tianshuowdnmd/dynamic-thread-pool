import cn.ts.config.Application;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author: ts
 * @description
 * @create: 2025/1/8 12:55
 */
@SpringBootTest(classes = Application.class)
public class ApiTest {

    @Resource
    private ThreadPoolExecutor threadPoolExecutor01;
    @Test
    void testThreadPool() throws Exception {

        Thread t = new Thread(() -> {
            try {
                while (true) {
                    Random random = new Random();
                    int startDuration = random.nextInt(5) + 1;
                    int runDuration = random.nextInt(10) + 1;

                    threadPoolExecutor01.submit(() -> {
                        try {
                            TimeUnit.SECONDS.sleep(startDuration);
                            System.out.printf("启动花费时间: %ds\n", startDuration);

                            TimeUnit.SECONDS.sleep(runDuration);
                            System.out.printf("运行花费时间: %ds\n", runDuration);

                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    });

                    Thread.sleep((random.nextInt(10) + 1) * 1000);
                }
            } catch (Exception e) {
            }
        });

        t.start();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        countDownLatch.await();
    }
}
