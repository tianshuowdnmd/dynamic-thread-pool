package cn.bugstack.middleware.dynamic.thread.pool.sdk.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashMap;

/**
 * @author: ts
 * @description
 * @create: 2025/1/18 14:59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlarmMessageDTO {
    private String message;
    private LinkedHashMap<String, String> parameters;
    private String remarks;

    public static AlarmMessageDTO buildAlarmMessageDTO(
            String message,
            LinkedHashMap<String, String > parameters,
            String remarks
    ) {
        return new AlarmMessageDTO(
                message,
                parameters,
                remarks
        );
    }

    public static AlarmMessageDTO buildAlarmMessageDTO(
            String message,
            String remarks
    ) {
        return AlarmMessageDTO.buildAlarmMessageDTO(
                message,
                new LinkedHashMap<>(),
                remarks
        );
    }

    public static AlarmMessageDTO buildAlarmMessageDTO(
            String message
    ) {
        return AlarmMessageDTO.buildAlarmMessageDTO(
                message,
                new LinkedHashMap<>(),
                null
        );
    }

    public <T> AlarmMessageDTO appendParameter(String k, T v) {
        parameters.put(
                k,
                v.toString()
        );
        return this;
    }
}
