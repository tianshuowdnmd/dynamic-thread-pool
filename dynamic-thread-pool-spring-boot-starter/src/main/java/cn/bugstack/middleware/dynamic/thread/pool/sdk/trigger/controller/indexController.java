package cn.bugstack.middleware.dynamic.thread.pool.sdk.trigger.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author: ts
 * @description
 * @create: 2025/1/6 11:35
 */
@Slf4j
@Controller
@RequestMapping("dynamicAdmin")
public class indexController {

    @GetMapping("/")
    public String index() {
        log.info("Loading index.html for /dynamic/thread/pool/admin/");
        return "forward:/dynamicAdmin/index.html";
    }

}
