package cn.bugstack.middleware.dynamic.thread.pool.sdk.trigger.job;

import java.util.concurrent.TimeUnit;

/**
 * @author: ts
 * @description
 * @create: 2025/1/18 15:44
 */
public class AlarmCanSendStateChangeJob {
    private Runnable task;

    private Thread thread;

    public AlarmCanSendStateChangeJob(Runnable task) {
        this.task = task;
    }

    public void run(int delay) {
        if (thread == null) {
            start(delay);
            return;
        }

        if (thread.isAlive()) {
            thread.interrupt();
        }

        start(delay);
    }

    private void start(int delay) {
        thread = new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(delay);
                task.run();
            } catch (Exception ignored) {}
        });
        thread.start();
    }

}
