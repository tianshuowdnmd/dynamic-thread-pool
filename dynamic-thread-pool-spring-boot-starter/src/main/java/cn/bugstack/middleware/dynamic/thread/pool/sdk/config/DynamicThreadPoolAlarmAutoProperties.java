package cn.bugstack.middleware.dynamic.thread.pool.sdk.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @author: ts
 * @description
 * @create: 2025/1/18 14:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = "dynamic.thread.pool.alarm")
public class DynamicThreadPoolAlarmAutoProperties {

    private Boolean enabled = false;
    private List<String > usePlatform;
    private AccessToken accessToken;
    private Integer alarmPercent;
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class AccessToken {
        private String dingDing;
        private String feiShu;
    }

}