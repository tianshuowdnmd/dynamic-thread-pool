package cn.bugstack.middleware.dynamic.thread.pool.sdk.domain.alarm.strategy;

import cn.bugstack.middleware.dynamic.thread.pool.sdk.api.dto.AlarmMessageDTO;
import com.taobao.api.ApiException;

/**
 * @author: ts
 * @description
 * @create: 2025/1/18 15:21
 */
public interface IAlarmStrategy {
    void send(AlarmMessageDTO message) throws ApiException;
}
