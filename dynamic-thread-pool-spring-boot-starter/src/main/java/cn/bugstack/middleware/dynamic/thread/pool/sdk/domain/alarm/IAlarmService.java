package cn.bugstack.middleware.dynamic.thread.pool.sdk.domain.alarm;

import cn.bugstack.middleware.dynamic.thread.pool.sdk.api.dto.AlarmMessageDTO;
import cn.bugstack.middleware.dynamic.thread.pool.sdk.domain.thread.model.entity.ThreadPoolConfigEntity;
import com.taobao.api.ApiException;

import java.util.List;

/**
 * @author: ts
 * @description
 * @create: 2025/1/18 14:45
 */
public interface IAlarmService {

    public  void send(AlarmMessageDTO message) throws ApiException;

    void sendIfThreadPoolHasDanger(List<ThreadPoolConfigEntity> pools);


}
