package cn.bugstack.middleware.dynamic.thread.pool.sdk.domain.alarm.strategy;

import cn.bugstack.middleware.dynamic.thread.pool.sdk.api.dto.AlarmMessageDTO;
import cn.bugstack.middleware.dynamic.thread.pool.sdk.domain.alarm.IAlarmService;
import com.taobao.api.ApiException;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @author: ts
 * @description
 * @create: 2025/1/18 14:56
 */
public abstract class AbstractAlarmStrategy implements IAlarmStrategy {

    private static HashMap<String, AbstractAlarmStrategy> strategiesMap = new HashMap<>();

    public abstract String getStrategyName();
    @PostConstruct
    private void initStrategy() {
        strategiesMap.put(getStrategyName(), this);
    }
    public static List<AbstractAlarmStrategy> selectStrategy(List<String> strategies) {
        ArrayList<AbstractAlarmStrategy> abstractAlarmStrategies = new ArrayList<>();
        strategies.forEach(strategy -> {
            abstractAlarmStrategies.add(strategiesMap.get(strategy));
        });
        return abstractAlarmStrategies;
    }

    public static List<AbstractAlarmStrategy> selectStrategy(String strategies) {
        return selectStrategy(Collections.singletonList(strategies));
    }


    protected String buildMessageContent(AlarmMessageDTO message) {
        StringBuilder content = new StringBuilder();
        HashMap<String, String> parameters = message.getParameters();
        String remarks = message.getRemarks();

        content.append("【监控告警】 ").append(message.getMessage()).append("\n");
        parameters.forEach((k, v) -> {
            content.append(" ").append(k).append(": ").append(v).append("\n");
        });
        if (remarks != null) {
            content.append(" ").append(remarks).append("\n");
        }
        return content.toString();
    }
}
