package cn.bugstack.middleware.dynamic.thread.pool.sdk.registry.redis;

import cn.bugstack.middleware.dynamic.thread.pool.sdk.domain.alarm.AlarmServiceImpl;
import cn.bugstack.middleware.dynamic.thread.pool.sdk.domain.alarm.IAlarmService;
import cn.bugstack.middleware.dynamic.thread.pool.sdk.domain.thread.model.entity.ThreadPoolConfigEntity;
import cn.bugstack.middleware.dynamic.thread.pool.sdk.domain.thread.model.valobj.RegistryEnumVO;
import cn.bugstack.middleware.dynamic.thread.pool.sdk.registry.IRegistry;
import org.redisson.api.RBucket;
import org.redisson.api.RList;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author Fuzhengwei bugstack.cn @小傅哥
 * @description Redis 注册中心
 * @create 2024-05-12 16:22
 */
public class RedisRegistry implements IRegistry {

    private final RedissonClient redissonClient;

    private final IAlarmService alarmService;

    public RedisRegistry(RedissonClient redissonClient, IAlarmService alarmService) {
        this.redissonClient = redissonClient;
        this.alarmService = alarmService;
    }

    @Override
    public void reportThreadPool(List<ThreadPoolConfigEntity> threadPoolEntities) {
        RList<ThreadPoolConfigEntity> list = redissonClient.getList(RegistryEnumVO.THREAD_POOL_CONFIG_LIST_KEY.getKey());
        RLock lock = redissonClient.getLock(RegistryEnumVO.REPORT_THREAD_POOL_CONFIG_LIST_REDIS_LOCK_KEY.getKey());


        if (AlarmServiceImpl.canSendForThreadPoolDanger()) {
            alarmService.sendIfThreadPoolHasDanger(threadPoolEntities);
        }

        String applicationName = threadPoolEntities.get(0).getAppName();
        try {
            boolean canHasLock = lock.tryLock(3000, 3000, TimeUnit.MILLISECONDS);

            if (canHasLock) {
                // 获取本应用线程池的开始索引
                int index = -1;
                for (int i = 0; i < list.size(); i++) {
                    ThreadPoolConfigEntity originalPoolConfig = list.get(i);
                    if (Objects.equals(originalPoolConfig.getAppName(), applicationName)) {
                        index = i;
                        break;
                    }
                }

                if (index == -1) {
                    // 如果不存在，直接添加到列表末尾
                    list.addAll(threadPoolEntities);
                } else {
                    // 更新线程池配置
                    for (ThreadPoolConfigEntity newPoolConfig : threadPoolEntities) {
                        ThreadPoolConfigEntity originalPoolConfig = list.get(index);
                        if (!originalPoolConfig.equals(newPoolConfig)) {
                            list.fastRemove(index);
                            list.add(index, newPoolConfig);
                        }
                        index++;
                    }
                }
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            if (lock.isHeldByCurrentThread()) {
                lock.unlock();
            }
        }
    }

    @Override
    public void reportThreadPoolConfigParameter(ThreadPoolConfigEntity threadPoolConfigEntity) {
        String cacheKey = RegistryEnumVO.THREAD_POOL_CONFIG_PARAMETER_LIST_KEY.getKey() + "_" + threadPoolConfigEntity.getAppName() + "_" + threadPoolConfigEntity.getThreadPoolName();
        RBucket<ThreadPoolConfigEntity> bucket = redissonClient.getBucket(cacheKey);
        bucket.set(threadPoolConfigEntity, Duration.ofDays(30));
    }

}
