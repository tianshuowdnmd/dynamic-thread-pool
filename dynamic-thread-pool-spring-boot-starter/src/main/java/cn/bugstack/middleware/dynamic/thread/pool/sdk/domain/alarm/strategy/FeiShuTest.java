package cn.bugstack.middleware.dynamic.thread.pool.sdk.domain.alarm.strategy;

import okhttp3.*;

import java.io.IOException;

public class FeiShuTest {
    public static void main(String[] args) {

//        String token = config.getAccessToken().getDingDing();
        // 飞书机器人的 Webhook 地址
        String webhookUrl = "https://open.feishu.cn/open-apis/bot/v2/hook/b8cccac7-6a1c-48c4-ab56-e3e765a272ef";

        // 创建 OkHttp 客户端
        OkHttpClient client = new OkHttpClient();

        // 构建 JSON 请求体
        String json = "{\"msg_type\":\"text\",\"content\":{\"text\":\"【监控告警】 超出线程池处理能力\\n" +
                "告警线程池数: 1\\n" +
                "应用名称: dynamic-thread-pool-test-app2\\n" +
                "线程池名称: threadPoolExecutor01\\n" +
                "池中线程数: 50\\n" +
                "核心线程数: 20\\n" +
                "最大线程数: 50\\n" +
                "活跃线程数: 50\\n" +
                "队列类型: LinkedBlockingQueue\\n" +
                "队列中任务数: 96\\n" +
                "队列剩余容量: 4\"}}";

        // 创建请求体
        RequestBody body = RequestBody.create(json, MediaType.parse("application/json"));

        // 创建请求
        Request request = new Request.Builder()
                .url(webhookUrl)
                .post(body)
                .build();

        // 发送请求并处理响应
        try (Response response = client.newCall(request).execute()) {
            System.out.println("Response Code: " + response.code());
            System.out.println("Response Body: " + response.body().string());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}