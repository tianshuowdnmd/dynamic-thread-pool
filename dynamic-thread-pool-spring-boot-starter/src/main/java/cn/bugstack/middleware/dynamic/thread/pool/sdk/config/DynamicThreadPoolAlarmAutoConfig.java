package cn.bugstack.middleware.dynamic.thread.pool.sdk.config;

import cn.bugstack.middleware.dynamic.thread.pool.sdk.domain.alarm.AlarmServiceImpl;
import cn.bugstack.middleware.dynamic.thread.pool.sdk.domain.alarm.IAlarmService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author: ts
 * @description
 * @create: 2025/1/18 14:43
 */
@Configuration
@ConditionalOnProperty(prefix = "dynamic.thread.pool.alarm", name = "enabled", havingValue = "true")
@EnableConfigurationProperties(DynamicThreadPoolAlarmAutoProperties.class)
@ComponentScan("cn.bugstack.middleware.dynamic.thread.pool.sdk.domain.alarm")
public class DynamicThreadPoolAlarmAutoConfig {

    @Bean
    public IAlarmService alarmService() {
        return new AlarmServiceImpl();
    }
}
