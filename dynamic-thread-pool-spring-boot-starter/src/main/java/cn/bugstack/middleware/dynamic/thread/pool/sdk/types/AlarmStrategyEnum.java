package cn.bugstack.middleware.dynamic.thread.pool.sdk.types;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author: ts
 * @description
 * @create: 2025/1/18 15:14
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
public enum AlarmStrategyEnum {

    DING_DING("DingDing", "钉钉"),
    FEI_SHU("FeiShu", "飞书");

    private String value;
    private String description;
}
