package cn.bugstack.middleware.dynamic.thread.pool.sdk.domain.alarm.strategy;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import cn.bugstack.middleware.dynamic.thread.pool.sdk.api.dto.AlarmMessageDTO;
import cn.bugstack.middleware.dynamic.thread.pool.sdk.config.DynamicThreadPoolAlarmAutoProperties;
import cn.bugstack.middleware.dynamic.thread.pool.sdk.types.AlarmStrategyEnum;
import com.taobao.api.ApiException;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: ts
 * @description
 * @create: 2025/1/18 17:58
 */
@Slf4j
@Component
public class FeiShuAlarmStrategy extends AbstractAlarmStrategy{
    @Resource
    private DynamicThreadPoolAlarmAutoProperties config;
    @Override
    public String getStrategyName() {
        return AlarmStrategyEnum.FEI_SHU.getValue();
    }

    @Override
    public void send(AlarmMessageDTO message) throws ApiException {
        String token = config.getAccessToken().getFeiShu();
        // 飞书机器人的 Webhook 地址
        String webhookUrl = "https://open.feishu.cn/open-apis/bot/v2/hook/" + token;

        // 创建 OkHttp 客户端
        OkHttpClient client = new OkHttpClient();
        String messageContent = buildMessageContent(message);
        // 构建 JSON 请求体
        String json = new String();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, Object> content = new HashMap<>();
            content.put("text", messageContent);

            Map<String, Object> jsonMap = new HashMap<>();
            jsonMap.put("msg_type", "text");
            jsonMap.put("content", content);

            json = objectMapper.writeValueAsString(jsonMap);
        } catch (Exception e) {
            log.error("构建 JSON 失败", e);
            throw new ApiException("构建 JSON 失败", e);
        }



        // 创建请求体
        RequestBody body = RequestBody.create(json, MediaType.parse("application/json"));

        // 创建请求
        Request request = new Request.Builder()
                .url(webhookUrl)
                .post(body)
                .build();

        // 发送请求并处理响应
        try (Response response = client.newCall(request).execute()) {
            System.out.println("Response Code: " + response.code());
            System.out.println("Response Body: " + response.body().string());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
