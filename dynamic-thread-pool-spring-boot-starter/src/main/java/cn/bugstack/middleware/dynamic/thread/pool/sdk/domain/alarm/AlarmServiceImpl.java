package cn.bugstack.middleware.dynamic.thread.pool.sdk.domain.alarm;

import cn.bugstack.middleware.dynamic.thread.pool.sdk.api.dto.AlarmMessageDTO;
import cn.bugstack.middleware.dynamic.thread.pool.sdk.config.DynamicThreadPoolAlarmAutoProperties;
import cn.bugstack.middleware.dynamic.thread.pool.sdk.domain.alarm.strategy.AbstractAlarmStrategy;
import cn.bugstack.middleware.dynamic.thread.pool.sdk.domain.thread.model.entity.ThreadPoolConfigEntity;
import cn.bugstack.middleware.dynamic.thread.pool.sdk.trigger.job.AlarmCanSendStateChangeJob;
import com.taobao.api.ApiException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author: ts
 * @description
 * @create: 2025/1/18 14:45
 */
@Slf4j
@EnableAsync
public class AlarmServiceImpl implements IAlarmService{

    private static Boolean canSendForThreadPoolDanger = true;
    @Resource
    private DynamicThreadPoolAlarmAutoProperties config;
    @Resource
    private ApplicationContext applicationContext;
    @Override
    public void send(AlarmMessageDTO message){

        log.info("告警推送: {}", message);

        Boolean enable = config.getEnabled();
        if (!enable) {
            log.info("告警推送未开启");
            return;
        }

        List<AbstractAlarmStrategy> strategies = AbstractAlarmStrategy.selectStrategy(config.getUsePlatform());

        strategies.forEach(strategy -> {
            try {
                strategy.send(message);
            } catch (ApiException e) {
                log.error("推送告警时发生错误: {} | {}", e.getErrCode(), e.getErrMsg());
            }
        });
    }

    @Override
    public void sendIfThreadPoolHasDanger(List<ThreadPoolConfigEntity> pools) {
        List<ThreadPoolConfigEntity> dangerPools = new ArrayList<>();
        for (ThreadPoolConfigEntity pool : pools) {
            // 活跃线程数达到最大 且 阻塞队列已满
            int currentPercent = pool.getQueueSize() * 100
                    /(pool.getQueueSize() + pool.getRemainingCapacity()) ;
            if (Objects.equals(pool.getActiveCount(), pool.getMaximumPoolSize())
                    && currentPercent >= config.getAlarmPercent()) {
                dangerPools.add(pool);
            }
        }

        if (dangerPools.isEmpty()) {
            return;
        }

        AlarmMessageDTO alarmMessageDTO = AlarmMessageDTO
                .buildAlarmMessageDTO("超出线程池处理能力")
                .appendParameter("告警线程池数", dangerPools.size());
        dangerPools.forEach(pool -> alarmMessageDTO
                .appendParameter("======", "======")
                .appendParameter("应用名称", pool.getAppName())
                .appendParameter("线程池名称", pool.getThreadPoolName())
                .appendParameter("池中线程数", pool.getPoolSize())
                .appendParameter("核心线程数", pool.getCorePoolSize())
                .appendParameter("最大线程数", pool.getMaximumPoolSize())
                .appendParameter("活跃线程数", pool.getActiveCount())
                .appendParameter("队列类型", pool.getQueueType())
                .appendParameter("队列中任务数", pool.getQueueSize())
                .appendParameter("队列剩余容量", pool.getRemainingCapacity())
        );
        send(alarmMessageDTO);

        AlarmServiceImpl.canSendForThreadPoolDanger = false;

        // 启动一个定时任务, 10分钟后自动将 canSendForThreadPoolDanger 改为true
        AlarmCanSendStateChangeJob job = new AlarmCanSendStateChangeJob(
                () -> AlarmServiceImpl.setCanSendForThreadPoolDanger(true)
        );
        job.run(60 * 10);
    }

    public static synchronized Boolean canSendForThreadPoolDanger() {
        return canSendForThreadPoolDanger;
    }

    public static synchronized void setCanSendForThreadPoolDanger(Boolean canSendForThreadPoolDanger) {
        AlarmServiceImpl.canSendForThreadPoolDanger = canSendForThreadPoolDanger;
    }
}
